copy_dependencies:
	mkdir -p ./build
	cp ./bin/amd64/nothing.sh ./build/pre-build
	cp ./bin/amd64/nothing.sh ./build/post-build
	cp ./bin/amd64/fing-5.1.0-amd64.tar.gz ./build/fing.tar.gz

cleanup_dependencies:
	rm -f ./build/pre-build
	rm -f ./build/post-build
	rm -f ./build/fing.tar.gz