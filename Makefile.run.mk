docker_run:
	docker run -d --name=mosquitto_test_run homesmarthome/mosquitto:latest
	docker run -d \
	  --name=network-analysis_test_run \
	  -v $(PWD)/test/env:/env \
	  --net host \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep network-analysis_test_run

docker_stop:
	docker rm -f network-analysis_test_run 2> /dev/null ; true
	docker rm -f mosquitto_test_run 2> /dev/null; true
